package fr.lsevere.ovhddns.main;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import fr.lsevere.ovhddns.lib.IpFetcher;
import fr.lsevere.ovhddns.lib.OvhDdnsClient;
import fr.lsevere.ovhddns.lib.UrlChecker;

public class Main {

	private final Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws IOException, ParseException {
		CommandLine line = parseArgs(args);
		initLogs(line);
		new Main().run(line);
	}

	private static CommandLine parseArgs(String[] args) {
		final Options options = configParameters();

		final CommandLineParser parser = new DefaultParser();
		CommandLine line = null;

		try {
			line = parser.parse(options, args);
		} catch (ParseException e) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("OVH DDNS", options, true);
			System.exit(0);
		}
		return line;

	}

	private void run(CommandLine line) throws IOException {
		log.info("======= Starting : " + new Date() + "=======");

		checkUrlIfAsked(line);
		String myIp = new IpFetcher().fetchIp();
		HttpStatus httpStatus = new OvhDdnsClient().changeDdns(myIp, line.getOptionValue("username"),
				line.getOptionValue("password"), line.getOptionValue("hostname"));
		log.info("Request result : " + httpStatus.value());
	}

	private static void initLogs(CommandLine line) {
		String logDir = line.getOptionValue("logPath");
		logDir = logDir.replace("\n", "");
		logDir = logDir.replace("\r", "");
		if (!logDir.endsWith(System.lineSeparator())) {
			logDir = logDir + System.lineSeparator();
		}
		System.setProperty("log.name", logDir);
	}

	private void checkUrlIfAsked(CommandLine line) {
		if (Boolean.TRUE.toString().toLowerCase().equals(line.getOptionValue("check").toLowerCase())) {
			if (!(line.hasOption("checkUser") && line.hasOption("checkPassword") && line.hasOption("checkHostname"))) {
				log.error("You have asked to check an url beforehand. Please add the -cu, -cp & -ch arguments.");
				System.exit(-1);
			}
			HttpStatus httpStatus = new UrlChecker().changeDdns(line.getOptionValue("checkHostname"),
					line.getOptionValue("checkUser"), line.getOptionValue("checkPassword"));
			if (!httpStatus.isError()) {
				log.info("The check has returned 200. Ending.");
				System.exit(0);
			}
		}
	}

	private static Options configParameters() {

		final Option usernameOption = Option.builder("u").longOpt("username").desc("Your OVH DDNS username")
				.hasArg(true).argName("username").required(true).build();

		final Option passwordOption = Option.builder("p").longOpt("password").desc("Your OVH DDNS password")
				.hasArg(true).argName("password").required(true).build();

		final Option hostnameOption = Option.builder("h").longOpt("hostname")
				.desc("The name of the domain corresponding to the DDNS login").hasArg(true).required(true).build();

		final Option checkOption = Option.builder("c").longOpt("check")
				.desc("(Check) [true/false] Use if you want the program to check an url before refreshing your IP")
				.hasArg(false).required(true).hasArg(true).build();

		final Option checkUserOption = Option.builder("cu").longOpt("checkUser")
				.desc("(Check) Username if you need basicAuth").hasArg(true).required(false).build();

		final Option checkPasswordOption = Option.builder("cp").longOpt("checkPassword")
				.desc("(Check) Password if you need basicAuth").hasArg(true).argName("password").required(false)
				.build();

		final Option checkHostnameOption = Option.builder("ch").longOpt("checkHostname")
				.desc("(Check) The url to check").hasArg(true).required(false).build();

		final Option logPathOption = Option.builder("l").longOpt("logPath").desc("The path to the log archive")
				.hasArg(true).required(true).build();

		final Options options = new Options();

		options.addOption(usernameOption);
		options.addOption(passwordOption);
		options.addOption(hostnameOption);

		options.addOption(checkOption);
		options.addOption(checkUserOption);
		options.addOption(checkPasswordOption);
		options.addOption(checkHostnameOption);

		options.addOption(logPathOption);

		return options;
	}
}
