package fr.lsevere.ovhddns.lib;

import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class OvhDdnsClient {

    private final Logger log = LoggerFactory.getLogger(OvhDdnsClient.class);
	
	public HttpStatus changeDdns(String ip, String login, String password, String hostname) {
		final String uri = "http://www.ovh.com/nic/update?system=dyndns&hostname={0}&myip={1}";
		String uriWorked = MessageFormat.format(uri, hostname, ip);
		HttpHeaders authHeaders = createHeaders(login, password);
		ResponseEntity<Void> result = this.getRestTemplate().exchange(uriWorked, HttpMethod.GET,
				new HttpEntity<Void>(authHeaders), Void.class);
		return result.getStatusCode();
	}

	private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate;
	}

	private HttpHeaders createHeaders(final String username, final String password) {
		return new HttpHeaders() {
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}
}
