package fr.lsevere.ovhddns.lib;

import java.nio.charset.Charset;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.lsevere.ovhddns.main.Main;

public class UrlChecker {

    private final Logger log = LoggerFactory.getLogger(Main.class);
	
	public HttpStatus changeDdns(String url, String login, String password) {
		
		HttpHeaders authHeaders = createHeaders(login, password);
		ResponseEntity<Void> result = this.getRestTemplate().exchange(url, HttpMethod.GET,
				new HttpEntity<Void>(authHeaders), Void.class);
		log.info("Request result : " + result.getStatusCodeValue());
		return result.getStatusCode();
	}

	private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate;
	}

	private HttpHeaders createHeaders(final String username, final String password) {
		return new HttpHeaders() {
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}
}
