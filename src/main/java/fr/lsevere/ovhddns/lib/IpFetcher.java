package fr.lsevere.ovhddns.lib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import fr.lsevere.ovhddns.model.IpData;

public class IpFetcher {

    private final Logger log = LoggerFactory.getLogger(IpFetcher.class);
	
	public String fetchIp() throws IOException {
		final String uri = "https://api.ipify.org/?format=json";

		IpData resultData = this.getRestTemplate().getForObject(uri, IpData.class);
		log.info("My public ip : " + resultData.getIp());
		return resultData.getIp();
	}

	private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		// Add the Jackson Message converter
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		// Note: here we are making this converter to process any kind of response,
		// not only application/*json, which is the default behaviour
		converter.setSupportedMediaTypes(Arrays.asList(new MediaType[] { MediaType.ALL }));
		messageConverters.add(converter);
		restTemplate.setMessageConverters(messageConverters);
		return restTemplate;
	}
}
