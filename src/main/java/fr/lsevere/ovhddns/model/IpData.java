package fr.lsevere.ovhddns.model;

import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IpData {

	Logger log = Logger.getGlobal();

	@JsonProperty("ip")
	private String ip;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
