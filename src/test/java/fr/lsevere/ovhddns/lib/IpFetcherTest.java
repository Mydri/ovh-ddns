package fr.lsevere.ovhddns.lib;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

public class IpFetcherTest {

	@Test
	public void shouldFetch() throws IOException {
		String result = new IpFetcher().fetchIp();
		assertNotNull(result);
		assertTrue(result.contains("."));
		System.out.println(result);
	}
}
